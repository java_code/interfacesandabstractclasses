package org.jrt.app.consoles;

import org.jrt.app.abstractClasses.Console;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NintendoTest {

    @Spy
    Nintendo console;

    @Test
    public void testTurnOn_turnOnIfPluggedInAndTurnedOff() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", false);
        console.turnOn();
        assertTrue(console.isPower());
    }

    @Test
    public void testTurnOn_leftOnIfPluggedInAndTurnedOn(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.turnOn();
        verify(console, never()).setPower(true);
        assertTrue(console.isPower());
    }

    @Test
    public void testTurnOn_leaveOffIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        Whitebox.setInternalState(console, "power", false);
        console.turnOn();
        assertFalse(console.isPower());
    }

    @Test
    public void testTurnOff_turnOffIfPluggedInAndTurnedOn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.turnOff();
        assertFalse(console.isPower());
    }

    @Test
    public void testTurnOff_leaveOffIfPowerIsOff(){
        Whitebox.setInternalState(console, "power", false);
        console.turnOff();
        verify(console, never()).setPower(false);
        assertFalse(console.isPower());
    }

    @Test
    public void testReset_resetIfPluggedInAndTurnedOn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.reset();

        InOrder inOrder = Mockito.inOrder(console);

        inOrder.verify(console).turnOff();
        inOrder.verify(console).turnOn();
    }

    @Test
    public void testReset_doNotResetIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        Whitebox.setInternalState(console, "power", false);
        console.reset();
        verify(console, never()).turnOff();
        verify(console, never()).turnOn();
        assertFalse(console.isPower());
    }

    @Test
    public void testReset_doNotResetIfNoPower(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", false);
        console.reset();
        verify(console, never()).turnOff();
        verify(console, never()).turnOn();
        assertFalse(console.isPower());
    }
}