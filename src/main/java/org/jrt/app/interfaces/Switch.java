package org.jrt.app.interfaces;

public interface Switch {
    String MESSAGE = "Random message from the switch interface";

    /**
     * turn on the power
     */
    void turnOn();

    /**
     * turn off the power
     */
    void turnOff();
}
