package org.jrt.app;

import org.jrt.app.abstractClasses.Console;
import org.jrt.app.consoles.Nintendo;
import org.jrt.app.consoles.Playstation;
import org.jrt.app.interfaces.Switch;

public class InterfacesAndAbstractClasses {

    public static void main(String[] args){

        System.out.println(Switch.MESSAGE);
        System.out.println();

        Console nintendo = new Nintendo();

        System.out.println("Nintendo:");
        nintendo.turnOn();
        nintendo.reset();
        nintendo.plugIn();
        nintendo.reset();
        nintendo.turnOn();
        nintendo.reset();
        nintendo.turnOff();
        nintendo.unplug();
        System.out.println();
        
        Console playstation = new Playstation();

        System.out.println("Playstation:");
        playstation.turnOn();
        playstation.reset();
        playstation.plugIn();
        playstation.reset();
        playstation.turnOn();
        playstation.reset();
        playstation.turnOff();
        playstation.unplug();
        
    }
}
