package org.jrt.app.abstractClasses;

import org.jrt.app.interfaces.Pluggable;
import org.jrt.app.interfaces.Switch;

public abstract class Console implements Pluggable, Switch {
    private boolean power = false;
    private boolean pluggedIn = false;

    /**
     *
     * @return true if the power is on, false if the power is off
     */
    public boolean isPower() {
        return power;
    }

    /**
     *
     * @param power {@link Console#isPower()} to be set
     */
    public void setPower(boolean power) {
        this.power = power;
    }

    /**
     *
     * @return true if plugged in, false if not plugged in
     */
    public boolean isPluggedIn() {
        return pluggedIn;
    }

    /**
     *
     * @param pluggedIn {@link Console#isPluggedIn()} to be set
     */
    public void setPluggedIn(boolean pluggedIn){
        this.pluggedIn = pluggedIn;
    }

    /**
     * reset the console
     */
    public abstract void reset();

    /**
     * plugin the console
     */
    @Override
    public void plugIn() {
        if(pluggedIn == false) {
            System.out.println("Plugging in console");
            pluggedIn = !pluggedIn;
        } else {
            System.out.println("Console is already plugged in");
        }
    }

    /**
     * unplug the console
     */
    @Override
    public void unplug() {
        if(pluggedIn){
            System.out.println("Unplugging the console");
            pluggedIn= !pluggedIn;
        } else {
            System.out.println("Console is already unplugged");
        }
    }
}
