package org.jrt.app.interfaces;

public interface Pluggable {
    /**
     * plug in the system
     */
    void plugIn();

    /**
     * unplug the system
     */
    void unplug();
}
