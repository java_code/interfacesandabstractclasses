package org.jrt.app.abstractClasses;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleTest {

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    Console console;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testPlugIn_plugsInIfNotPluggedIn() {
        Whitebox.setInternalState(console, "pluggedIn", false);
        console.plugIn();
        assertTrue(console.isPluggedIn());
    }

    @Test
    public void testPlugIn_doesNotPluginIfPluggedIn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        console.plugIn();
        assertTrue(console.isPluggedIn());
        assertEquals("Console is already plugged in", outContent.toString().trim());
    }



    @Test
    public void testUnplug_unplugsIfPluggedIn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        console.unplug();
        assertFalse(console.isPluggedIn());
    }

    @Test
    public void testUnplug_doesNotUnplugIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        console.unplug();
        assertFalse(console.isPluggedIn());
        assertEquals("Console is already unplugged", outContent.toString().trim());
    }
}