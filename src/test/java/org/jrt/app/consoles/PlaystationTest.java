package org.jrt.app.consoles;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PlaystationTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
    @Spy
    Playstation console;

    @Test
    public void testTurnOn_turnOnIfPluggedInAndTurnedOff() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", false);
        console.turnOn();
        assertTrue(console.isPower());
    }

    @Test
    public void testTurnOn_leftOnIfPluggedInAndTurnedOn(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.turnOn();
        verify(console, never()).setPower(true);
        assertTrue(console.isPower());
    }

    @Test
    public void testTurnOn_plugsInAndTurnsOnIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        Whitebox.setInternalState(console, "power", false);
        console.turnOn();
        assertTrue(console.isPluggedIn());
        assertTrue(console.isPower());
    }

    @Test
    public void testTurnOff_turnOffIfPluggedInAndTurnedOn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.turnOff();
        assertFalse(console.isPower());
    }

    @Test
    public void testTurnOff_leaveOffIfPowerIsOff(){
        Whitebox.setInternalState(console, "power", false);
        console.turnOff();
        verify(console, never()).setPower(false);
        assertFalse(console.isPower());
    }

    @Test
    public void testReset_resetIfPluggedInAndTurnedOn() {
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.reset();

        InOrder inOrder = Mockito.inOrder(console);

        inOrder.verify(console).turnOff();
        inOrder.verify(console).turnOn();
    }

    @Test
    public void testReset_doNotResetIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        Whitebox.setInternalState(console, "power", false);
        console.reset();
        verify(console, never()).turnOff();
        verify(console, never()).turnOn();
        assertFalse(console.isPower());
    }

    @Test
    public void testReset_doNotResetIfNoPower(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", false);
        console.reset();
        verify(console, never()).turnOff();
        verify(console, never()).turnOn();
        assertFalse(console.isPower());
    }

    @Test
    public void testUnplug_unplugsIfPluggedInAndNoPower(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", false);
        console.unplug();
        verify(console, never()).turnOff();
        assertFalse(console.isPluggedIn());
    }

    @Test
    public void testUnplug_turnoffAndUnplugsIfPluggedInAndPower(){
        Whitebox.setInternalState(console, "pluggedIn", true);
        Whitebox.setInternalState(console, "power", true);
        console.unplug();
        assertFalse(console.isPower());
        assertFalse(console.isPluggedIn());
    }

    @Test
    public void testUnplug_doesNotUnplugIfUnplugged(){
        Whitebox.setInternalState(console, "pluggedIn", false);
        console.unplug();
        assertFalse(console.isPluggedIn());
        assertEquals("Console is already unplugged", outContent.toString().trim());
    }


}