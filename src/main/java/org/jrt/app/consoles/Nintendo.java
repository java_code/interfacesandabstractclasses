package org.jrt.app.consoles;

import org.jrt.app.abstractClasses.Console;
import org.jrt.app.interfaces.Switch;

public class Nintendo extends Console {

    @Override
    public void turnOn(){
        if(super.isPluggedIn()) {
            if (!super.isPower()) {
                System.out.println("Turning on console");
                super.setPower(!super.isPower());
            } else {
                System.out.println("The power is already turned on");
            }
        } else {
            System.out.println("You must plug in the console before you can turn it on");
        }
    }

    @Override
    public void turnOff(){
        if(super.isPower()){
            System.out.println("Turning off the console");
            super.setPower(!super.isPower());
        } else {
            System.out.println("Power is already off");
        }
    }

    @Override
    public void reset(){
        if(super.isPluggedIn()){
            if(super.isPower()){
                System.out.println("Resetting the console by:");
                turnOff();
                turnOn();
                System.out.println("Console has been reset");
            } else {
                System.out.println("Power is  off. Power must be on to reset the console");
            }
        } else {
            System.out.println("You must plug in the console before you can reset it");
        }
    }
}
